const getBundleIdentifier = (variant) => {
  switch (variant) {
    case 'development':
       return 'in.letstransport.dev'; 
       
  
    case 'staging':
        return 'in.letstransport.stage';
    
    case 'production':
        return 'in.letstransport.prod';    
  }  
}

module.exports =  {
    "expo": {
      "name": "FlavoursPOC",
      "slug": "FlavoursPOC",
      "version": "1.0.0",
      "orientation": "portrait",
      "icon": "./assets/icon.png",
      "userInterfaceStyle": "light",
      "splash": {
        "image": "./assets/splash.png",
        "resizeMode": "contain",
        "backgroundColor": "#ffffff"
      },
      "assetBundlePatterns": [
        "**/*"
      ],
      "ios": {
        "supportsTablet": true,
        "bundleIdentifier": getBundleIdentifier(process.env.APP_VARIANT),
      },
      "android": {
        "adaptiveIcon": {
          "foregroundImage": "./assets/adaptive-icon.png",
          "backgroundColor": "#ffffff",
        },
        "package": getBundleIdentifier(process.env.APP_VARIANT),
      },
      "web": {
        "favicon": "./assets/favicon.png",
      },
      "extra": {
        "eas": {
          "projectId": "503a0ee3-7db9-4c35-9ae2-c119f4ca5990"
        },
        "apiUrl": process.env.SOME_URL,
      }
    }
  }
  